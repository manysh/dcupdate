package manish.dc.Service;

import manish.dc.Model.User;

import java.util.List;

public interface UserService {

//    User saveUser(User user,int regionId) throws Exception;

    List<User> findAllByRegion(int regionId) throws Exception;

    User findUserByEmail(String email);

    User Saveuser(User user);
}
