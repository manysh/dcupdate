package manish.dc.Controller;

import manish.dc.Model.Region;
import manish.dc.Model.Role;
import manish.dc.Repository.RegionRepository;
import manish.dc.Repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

@Controller
@RequestMapping(value = "/set")
public class SetupController {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RegionRepository regionRepository;

    @GetMapping(value = "/role")
    public String RoleSetup() throws SQLException {
        List<Role> roles = roleRepository.findAll();
        System.out.println(roles);
        if (roles.isEmpty()) {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/dcnew?useSSL=false","root","");
            PreparedStatement statement = connection.prepareStatement("INSERT INTO role(role_id,role)VALUES (?,?)");

            statement.setInt(1,1);
            statement.setString(2,"SUPERADMIN");
            statement.execute();

            statement.setInt(1,2);
            statement.setString(2,"ADMIN");
            statement.execute();

            statement.setInt(1,3);
            statement.setString(2,"CREATER");
            statement.execute();

            statement.setInt(1,4);
            statement.setString(2,"VIEWER");
            statement.execute();

            return "/message/added";
        }else{
            return "message/init";
        }
    }

    @GetMapping(value = "/region")
    public String RegionSetup() throws SQLException {
        List<Region> regions = regionRepository.findAll();
        System.out.println(regions);
        if (regions.isEmpty()) {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/dcnew?useSSL=false","root","");
            PreparedStatement statement = connection.prepareStatement("INSERT INTO region(id,detail,name)VALUES (?,?,?)");

            statement.setInt(1,1);
            statement.setString(2,"for super admin");
            statement.setString(3,"Super Head Brach");
            statement.execute();

            return "/message/added";
        }else{
            return "message/init";
        }
    }
}
