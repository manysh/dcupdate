package manish.dc.Controller;

import manish.dc.Model.Role;
import manish.dc.Model.User;
import manish.dc.Repository.RegionRepository;
import manish.dc.Repository.RoleRepository;
import manish.dc.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(value = "/registration")
public class RegistrationController {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    private UserService userService;

    @GetMapping
    public String registerNew(Model model){
        List<Role> role = roleRepository.findAll();
        System.out.println(role);
        model.addAttribute("role", role);
        model.addAttribute("region", regionRepository.findAll());
        model.addAttribute("user", new User());
        return "registration";
    }

    @PostMapping
    public String createNewUser(@Valid User user, BindingResult bindingResult, Model model) {

        User userExists = userService.findUserByEmail(user.getEmail());
        if (userExists != null) {
            bindingResult
                    .rejectValue("email", "error.user",
                            "There is already a user registered with the email provided");
        }
        if (bindingResult.hasErrors()) {
            model.addAttribute("errormsg", "field invalid");
            return "admin/user/create";
        }

        System.out.println(user);
        userService.Saveuser(user);
        model.addAttribute("successMessage", "User has been registered successfully");
        return "testPage";
    }
}
